package main

import (
	"strings"
	"testing"
	"time"
)

// https://stackoverflow.com/questions/31595791/how-to-test-panics
func shouldPanic(t *testing.T, f func()) {
	t.Helper()
	defer func() { _ = recover() }()
	f()
	t.Errorf("should have panicked")
}

func TestStringChomper(t *testing.T) {
	str := ChompableString("abcd")
	head := str.Chomp(1)
	if head != "a" || string(str) != "bcd" {
		t.Fatalf("chomp 1 produced %v, %v", head, str)
	}
	head = str.Chomp(2)
	if head != "bc" {
		t.Fatalf("chomp 2 produced %v, %v", head, str)
	}
	shouldPanic(t, func() { str.Chomp(2) })
	head = str.Chomp(1)
	if head != "d" {
		t.Fatalf("chomp 4 produced %v, %v", head, str)
	}
}

func TestFlowsParserBasic(t *testing.T) {
	example := "RF0027003201000000AS3112299901062022ATO01Y0000005"
	reader := strings.NewReader(example)
	flows, err := readFlowsFile(reader)
	if err != nil {
		t.Fatalf("error parsing flows: %s", err)
	}
	flow := Flow{UpdateType: 82, OriginCode: "0027", DestinationCode: "0032", RouteCode: "01000", StatusCode: "000", IsConcatenation: false, IsReversible: false, EndDate: time.Date(1, time.January, 1, 0, 0, 0, 0, time.UTC), StartDate: time.Date(2022, time.June, 1, 0, 0, 0, 0, time.Local), TocCode: "ATO", CrossLondon: 0, Discount: 1, Published: true, Id: "0000005"}
	if flows.Flows[0] != flow {
		t.Fatalf("parsed flow wrong: %#v", flows.Flows[0])
	}
}
