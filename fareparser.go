package main

import (
	"archive/zip"
	"bufio"
	"fmt"
	"io"
	"log"
	"strconv"
	"strings"
	"time"
)

type ChompableString string

func (c *ChompableString) Chomp(count int) string {
	if len(*c) < count {
		panic(fmt.Sprintf("asked to chomp(\"%s\", %d)", *c, count))
	}
	ret := string(*c)[:count]
	*c = ChompableString(string(*c)[count:])
	return ret
}

type UpdateMarker int

const (
	UpdateRefresh UpdateMarker = 'R'
	UpdateInsert  UpdateMarker = 'I'
	UpdateAmend   UpdateMarker = 'A'
	UpdateDelete  UpdateMarker = 'D'
)

type CrossLondonIndicator int

const (
	NotViaLondon          CrossLondonIndicator = 0
	LondonInclUnderground CrossLondonIndicator = 1
	LondonExclUnderground CrossLondonIndicator = 2
	LondonThameslink      CrossLondonIndicator = 3
)

type DiscountIndicator int

const (
	RailwayStandard    DiscountIndicator = 0
	RailwayNonstandard DiscountIndicator = 1
	PrivateStandard    DiscountIndicator = 2
	PrivateNonstandard DiscountIndicator = 3
)

// A simple fare record.
type Flow struct {
	// Indicates whether the record is to be inserted, amended, or deleted.
	// For full update files, is set to UpdateRefresh.
	UpdateType UpdateMarker
	// The 4-digit NLC code representing the flow origin.
	// Can be a cluster NLC.
	OriginCode string
	// The 4-digit NLC code representing the flow destination.
	// Can be a cluster NLC.
	DestinationCode string
	// Route code.
	RouteCode string
	// 3-digit status code, of uncertain meaning.
	StatusCode string
	// Whether this flow has been made by joining together other flows.
	IsConcatenation bool
	// Whether this flow can be used in the other direction (origin
	// and destination swapped).
	IsReversible bool
	// Last date this fare can be used on.
	// NOTE: This might be the zero value, if one isn't defined.
	EndDate time.Time
	// First date this fare can be used on.
	StartDate time.Time
	// The TOC code of the TOC setting the fare.
	TocCode string
	// Whether this fare is valid via London, and how.
	CrossLondon CrossLondonIndicator
	// Whether non-standard discounts apply.
	Discount DiscountIndicator
	// Whether this flow is published in the National Fares Manual.
	Published bool
	// A unique ID for this flow.
	Id string
}

// A fare for a flow.
type FlowFare struct {
	// The flow this fare is for.
	FlowId string
	// The 3-character ticket code for this fare.
	TicketCode string
	// The price of this fare, in pence.
	PricePence int32
	// The restriction code for this fare.
	RestrictionCode string
}

type FlowsData struct {
	Flows []Flow
	Fares []FlowFare
}

// convertDate takes a date in the format "ddmmyyyy" and converts it into a time.Time.
// If the date is 'high' ("31122999"), a zero value is returned.
func convertDate(date string) (time.Time, error) {
	if len(date) != 8 {
		panic(fmt.Sprintf("invalid date: %s", date))
	}
	var ret time.Time
	if date == "31122999" {
		return ret, nil
	}
	dd, err := strconv.Atoi(date[0:2])
	if err != nil {
		return ret, fmt.Errorf("failed to parse date: %w", err)
	}
	mm, err := strconv.Atoi(date[2:4])
	if err != nil {
		return ret, fmt.Errorf("failed to parse date: %w", err)
	}
	yyyy, err := strconv.Atoi(date[4:])
	if err != nil {
		return ret, fmt.Errorf("failed to parse date: %w", err)
	}
	return time.Date(yyyy, time.Month(mm), dd, 0, 0, 0, 0, time.Local), nil
}

func readFlowsFile(file io.Reader) (FlowsData, error) {
	scanner := bufio.NewScanner(file)
	var ret FlowsData
	flows := make([]Flow, 0)
	fares := make([]FlowFare, 0)
	for scanner.Scan() {
		line := string(scanner.Bytes())
		if strings.HasPrefix(line, "/!!") {
			// it's a comment, skip it
			continue
		}
		str := ChompableString(line)
		marker := str.Chomp(1)[0]
		recordType := str.Chomp(1)[0]
		switch recordType {
		case 'F':
			// flow record
			if len(line) != 49 {
				// not the correct record length
				return ret, fmt.Errorf("wrong length flow: %s", line)
			}
			var flow Flow
			flow.UpdateType = UpdateMarker(marker)
			flow.OriginCode = str.Chomp(4)
			flow.DestinationCode = str.Chomp(4)
			flow.RouteCode = str.Chomp(5)
			flow.StatusCode = str.Chomp(3)
			flow.IsConcatenation = str.Chomp(1) == "G"
			flow.IsReversible = str.Chomp(1) == "R"
			endDate, err := convertDate(str.Chomp(8))
			if err != nil {
				return ret, fmt.Errorf("parsing end date in %s: %w", line, err)
			}
			flow.EndDate = endDate
			startDate, err := convertDate(str.Chomp(8))
			if err != nil {
				return ret, fmt.Errorf("parsing start date in %s: %w", line, err)
			}
			flow.StartDate = startDate
			flow.TocCode = str.Chomp(3)
			xli, err := strconv.Atoi(str.Chomp(1))
			if err != nil {
				return ret, fmt.Errorf("parsing cross london ind in %s: %w", line, err)
			}
			flow.CrossLondon = CrossLondonIndicator(xli)
			dc, err := strconv.Atoi(str.Chomp(1))
			if err != nil {
				return ret, fmt.Errorf("parsing discount ind in %s: %w", line, err)
			}
			flow.Discount = DiscountIndicator(dc)
			flow.Published = str.Chomp(1) == "Y"
			flow.Id = str.Chomp(7)
			flows = append(flows, flow)
		case 'T':
			// fare record
			if len(line) != 22 {
				return ret, fmt.Errorf("wrong length fare record: %s", line)
			}
			var fare FlowFare
			fare.FlowId = str.Chomp(7)
			fare.TicketCode = str.Chomp(3)
			price, err := strconv.Atoi(str.Chomp(8))
			if err != nil {
				return ret, fmt.Errorf("parsing fare in pence in %s: %w", line, err)
			}
			fare.PricePence = int32(price)
			fare.RestrictionCode = str.Chomp(2)
			fares = append(fares, fare)
		default:
			return ret, fmt.Errorf("unknown type %c in %s", recordType, line)
		}
	}

	return FlowsData{
		Flows: flows,
		Fares: fares,
	}, nil
}

func ParseFaresData(path string) error {
	zip, err := zip.OpenReader(path)
	if err != nil {
		return fmt.Errorf("failed to open zip file %s: %w", path, err)
	}
	defer zip.Close()

	for _, file := range zip.File {
		log.Printf("found file %s", file.Name)
		zf, err := file.Open()
		if err != nil {
			return fmt.Errorf("failed to open file %s: %w", file.Name, err)
		}
		defer zf.Close()
		if strings.HasSuffix(file.Name, "FFL") {
			flows, err := readFlowsFile(zf)
			if err != nil {
				return fmt.Errorf("failed to read flows file: %w", err)
			}
			log.Printf("flows: %#v", flows)
		}
	}
	return nil
}
